package org.silveradiance.compositelaunch;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.ProgressMonitor;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchDelegate;
import org.eclipse.debug.core.Launch;
import org.eclipse.debug.core.model.ILaunchConfigurationDelegate;
import org.eclipse.debug.internal.core.LaunchManager;
import org.eclipse.debug.ui.DebugUITools;

public class CompositeLaunchDelegate implements ILaunchConfigurationDelegate {
	private LaunchManager mLaunchManager = (LaunchManager) DebugPlugin.getDefault().getLaunchManager();

	public CompositeLaunchDelegate() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void launch(ILaunchConfiguration configuration, String mode, ILaunch launch, IProgressMonitor monitor)
			throws CoreException {
		// TODO Auto-generated method stub
		
		List<String> selectedLaunches = (List<String>)configuration.getAttribute("selectedlaunches", new ArrayList<String>());
		
		List<ILaunchConfiguration> mLaunchConfigurationList = new ArrayList<ILaunchConfiguration>();
		for(int i = 0; i < selectedLaunches.size(); i++){
			mLaunchConfigurationList.add(decodeMemento(selectedLaunches.get(i)));
		}

//		try {
//			mLaunchConfigurationArr = DebugPlugin.getDefault().getLaunchManager().getLaunchConfigurations();
//		} catch (CoreException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		if (mLaunchConfigurationList.size() != 0) {
			for (ILaunchConfiguration k : mLaunchConfigurationList) {
				
				Set<String> set = new HashSet<String>();
				set.add("run");
//				set.add("debug");
				ILaunch mLaunch = new Launch(k,
						mLaunchManager.generateLaunchConfigurationName(k.getType().getIdentifier()), null);
				ILaunchDelegate[] mLaunchDelegates = k.getType().getDelegates(set);
				SubMonitor mProgressMonitor = SubMonitor.convert(monitor, 100);
				if (mLaunchDelegates.length != 0 && mLaunchManager.getLaunches().length < 3) {
					extracted(mLaunch);
					mLaunchDelegates[0].getDelegate().launch(k, "debug", mLaunch, mProgressMonitor);
					

				}
				System.out.println(k.getName());

			}
		}
	}

	private void extracted(ILaunch mLaunch) {
		mLaunchManager.addLaunch(mLaunch);
	}
	
	private ILaunchConfiguration decodeMemento(String memento) throws CoreException{
		
	return  DebugPlugin.getDefault().getLaunchManager().getLaunchConfiguration(memento);
	}
	
	
	

}

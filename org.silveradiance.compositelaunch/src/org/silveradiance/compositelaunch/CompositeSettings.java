package org.silveradiance.compositelaunch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.internal.ui.launchConfigurations.LaunchConfigurationPresentationManager;
import org.eclipse.debug.ui.AbstractLaunchConfigurationTab;
import org.eclipse.debug.ui.ILaunchConfigurationDialog;
import org.eclipse.debug.ui.ILaunchConfigurationTab;
import org.eclipse.debug.ui.ILaunchConfigurationTabGroup;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

public class CompositeSettings extends AbstractLaunchConfigurationTab  {
	protected static final String NAME_STRING = "Composite";
	
	protected static final String[] TABLE_COLUMNS = { "Config Name", "Type", "Col 3", "Col 4" };
	
	private List<ILaunchConfiguration> fLaunchConfigurationList; 
	
	private List<ILaunchConfiguration> fSavedCheckedLaunchConfigurationArr; 
	
	private List<ILaunchConfiguration> fCurrentlyCheckedLaunchConfigurationArr; 

	
	private SashForm fControl;
	
	private Table fLaunchesTable;
	
	public CompositeSettings(){
		
	}
	@Override
	public void createControl(Composite parent) {
		// TODO Auto-generated method stub
		
		
		GridData gd = new GridData(GridData.FILL_BOTH);
		gd.horizontalSpan = 2;
		SashForm sash = new SashForm(parent, SWT.SMOOTH);
		sash.setOrientation(SWT.HORIZONTAL);
		sash.setLayoutData(gd);
		sash.setFont(parent.getFont());
		sash.setVisible(true);
		fControl = sash;
		
		try {
			fLaunchesTable = createConfigsTable(fControl);
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		gd = new GridData(GridData.FILL_VERTICAL);
		fLaunchesTable.setLayoutData(gd);
		
		Composite editAreaComp = new Composite(fControl,SWT.SMOOTH);
		gd = new GridData(GridData.FILL_BOTH);
		editAreaComp.setLayoutData(gd);
		
			
		
	}

	@Override
	public Control getControl() {
		// TODO Auto-generated method stub
		return fControl;
	}

	@Override
	public void setDefaults(ILaunchConfigurationWorkingCopy configuration) {
		// TODO Auto-generated method stub
		List <String> mementoPack = new ArrayList<String>();
		configuration.setAttribute("selectedlaunches", mementoPack);
		try {
			configuration.doSave();
			fSavedCheckedLaunchConfigurationArr = new ArrayList<ILaunchConfiguration>(fCurrentlyCheckedLaunchConfigurationArr);
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void initializeFrom(ILaunchConfiguration configuration) {
		// TODO Auto-generated method stub
		List<String> selectedLaunches = new ArrayList<String>();
 
		
		try {
			selectedLaunches = (List<String>)configuration.getAttribute("selectedlaunches", new ArrayList<String>());
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		List<ILaunchConfiguration> mLaunchConfigurationList = new ArrayList<ILaunchConfiguration>();
		for(int i = 0; i < selectedLaunches.size(); i++){
			try {
				mLaunchConfigurationList.add(decodeMemento(selectedLaunches.get(i)));
			} catch (CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		fSavedCheckedLaunchConfigurationArr = new ArrayList<ILaunchConfiguration>(mLaunchConfigurationList);
		fCurrentlyCheckedLaunchConfigurationArr = new ArrayList<ILaunchConfiguration>(mLaunchConfigurationList);

		if(mLaunchConfigurationList.size() == 0) return;
		for(TableItem mItem : fLaunchesTable.getItems()){
			
    		if(mLaunchConfigurationList.stream()
    			    .filter(p -> p.getName().equals(mItem.getText(0))).collect(Collectors.toList()).size() > 0) mItem.setChecked(true);
			
		}
		
		scheduleUpdateJob();
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void performApply(ILaunchConfigurationWorkingCopy configuration) {
		// TODO Auto-generated method stub
		
		
		List <String> mementoPack = new ArrayList<String>();
		
		for(ILaunchConfiguration tmpLaunchConfiguration : fCurrentlyCheckedLaunchConfigurationArr){
			try {
				mementoPack.add(tmpLaunchConfiguration.getMemento());
			
			} catch (CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		configuration.setAttribute("selectedlaunches", mementoPack);
		try {
			configuration.doSave();
			fSavedCheckedLaunchConfigurationArr = new ArrayList<ILaunchConfiguration>(fCurrentlyCheckedLaunchConfigurationArr);
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public String getErrorMessage() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getMessage() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isValid(ILaunchConfiguration launchConfig) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean canSave() {
		// TODO Auto-generated method stub
//		return !listEqualsNoOrder(fSavedCheckedLaunchConfigurationArr,fCurrentlyCheckedLaunchConfigurationArr);
		return true;
	}

	@Override
	public String getName() {
		return NAME_STRING;
	}
	
	@Override
	public String getId() {
		return "org.silveradiance.compositelaunch.CompositeSetting"; //$NON-NLS-1$
	}

	@Override
	public Image getImage() {
		// TODO Auto-generated method stub
		return null;
	}

	
	private Table createConfigsTable(Composite parent) 
			throws CoreException{
		fLaunchConfigurationList = Arrays.asList(DebugPlugin.getDefault().getLaunchManager().getLaunchConfigurations());
		
		Table table = new Table(parent, SWT.CHECK | SWT.BORDER | SWT.V_SCROLL
		        | SWT.H_SCROLL);
		    table.setHeaderVisible(true);


		    for (int loopIndex = 0; loopIndex < TABLE_COLUMNS.length; loopIndex++) {
		      TableColumn column = new TableColumn(table, SWT.NULL);
		      column.setText(TABLE_COLUMNS[loopIndex]);
		    }

		    for (int loopIndex = 0; loopIndex < fLaunchConfigurationList.size(); loopIndex++) {
		      TableItem item = new TableItem(table, SWT.NULL);
		      item.setText("Item " + loopIndex);
		      item.setText(0, fLaunchConfigurationList.get(loopIndex).getName());
		      item.setText(1, fLaunchConfigurationList.get(loopIndex).getType().getName());
		      item.setText(2, "No");
		      item.setText(3, "A table item");
		    }

		    for (int loopIndex = 0; loopIndex < TABLE_COLUMNS.length; loopIndex++) {
		      table.getColumn(loopIndex).pack();
		    }

//		    table.setBounds(25, 25, 220, 200);
		    
		    table.addListener(SWT.Selection, new Listener() {
		      public void handleEvent(Event event) {
		        if (event.detail == SWT.CHECK) {
//		          text.setText("You checked " + event.item);
		        	TableItem item =(TableItem) event.item;
		        	if(item.getChecked()){
		        		List<ILaunchConfiguration> addList = fLaunchConfigurationList.stream()
		        			    .filter(p -> p.getName().equals(item.getText(0))).collect(Collectors.toList());
		        		for(ILaunchConfiguration k : addList){
		        			fCurrentlyCheckedLaunchConfigurationArr.add(k);
		        		}
		        	}else{
		        		List<ILaunchConfiguration> addList = fLaunchConfigurationList.stream()
		        			    .filter(p -> p.getName().equals(item.getText(0))).collect(Collectors.toList());
		        		for(ILaunchConfiguration k : addList){
		        			fCurrentlyCheckedLaunchConfigurationArr.remove(k);
		        			
		        		}
		        		
		        	}
		        	
		        } else {
		       
				  TableItem[] selected = table.getSelection();
				  System.out.println("Selected");
//		          text.setText("You selected " + event.item);
		        }
		      }
		    });
			return table;

		
		
	}
	
	private ILaunchConfiguration decodeMemento(String memento) throws CoreException{
		
	return  DebugPlugin.getDefault().getLaunchManager().getLaunchConfiguration(memento);
	}
	
	public static <T> boolean listEqualsNoOrder(List<T> l1, List<T> l2) {
	    final Set<T> s1 = new HashSet<>(l1);
	    final Set<T> s2 = new HashSet<>(l2);

	    return s1.equals(s2);
	}
	
	
	




}
